# Napredni algoritmi i strukture podataka lab1

### Zadatci za 5 bodova
1) Napisati program za izgradnju uravnoteženog stabla promišljenim redoslijedom upisa podataka.
2) Za program „SkipList“ dostupan na stranici predmeta (još bolje vlastiti!) napisati funkciju koja će
izračunati stupanj novog čvora učinkovitijim postupkom.
3) Napisati program koji će uravnotežiti zadano stablo (može i „hard kodirano“) DSW algoritmom.
4) Usporediti prosječnu brzinu pristupa nekom podatku u preskočnoj listi i binarnom stablu. Programe za
izgradnju i pretraživanje liste i stabla slobodno preuzmite sa stranica predmeta NASP i ASP ili bilo kojih
drugih izvora, a stablo može biti i „hard kodirano“ (dakle bez korisničkog unosa podataka).
Napomena: voditi računa o usporedivosti tih struktura s obzirom na građu!

### Zadatci za 11 bodova
1) Napisati program koji učitava niz prirodnih brojeva iz ASCII datoteke (po pretpostavci, datoteka nije
prazna) i upisuje ih u (inicijalno prazno) AVL stablo istim redoslijedom kao u datoteci. Program može
biti konzolni ili s grafičkim sučeljem, po vlastitom izboru. Konzolni program naziv ulazne datoteke treba
primiti prilikom pokretanja kao (jedini) argument s komandne linije, a grafički iz odgovarajućeg sučelja
po pokretanju programa. Nakon upisa svih podataka, ispisati izgrađeno stablo na standardni izlaz
(monitor). Program zatim treba omogućiti dodavanje novih čvorova te nakon svake promjene treba
ponovo ispisati stablo.
2) Napisati program koji učitava niz prirodnih brojeva iz ASCII datoteke (po pretpostavci, datoteka nije
prazna) i upisuje ih u (inicijalno prazno) crveno-crno (RB) stablo istim redoslijedom kao u datoteci.
Program može biti konzolni ili s grafičkim sučeljem, po vlastitom izboru. Konzolni program naziv ulazne
datoteke treba primiti prilikom pokretanja kao (jedini) argument s komandne linije, a grafički iz
odgovarajućeg sučelja po pokretanju programa. Nakon upisa svih podataka, ispisati izgrađeno stablo na
standardni izlaz (monitor) pri čemu treba nekako naznačiti boje čvorova. Program zatim treba omogućiti
dodavanje novih čvorova te nakon svake promjene treba ponovo ispisati stablo.

Napomena: datoteka treba biti tekstualna, a brojevi u datoteci odvojeni po jednim razmakom (space). Kraj
datoteke treba biti označen standardnim EOF znakom.

### Zadatci za 17 bodova
Isto kao zadatci za 11 bodova, samo treba dodati i brisanje čvora iz stabla.
