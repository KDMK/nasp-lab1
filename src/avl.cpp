#include<cstdio>
#include<cmath>

class Node {
  private:
    Node* leftChild;
    Node* rightChild;
    int height;
    int value;
  public:
    Node getLeftChild() {
      return *(this->leftChild);
    }

    Node getRightChild() {
      return *(this->rightChild);
    }

    int getHeight() {
      return this->height;
    }

    int getValue() {
      return this->value;
    }

    void setLeftChild(Node child) {
      this->leftChild = &child;
    }

    void setRightChild(Node child) {
      this->rightChild = &child;
    }

    void setValue(int value) {
      this->value = value;
    }

    void setHeight(int height) {
      this->height = height;
    }
};

void adjustHeight(Node node) {
  if(&node != nullptr) {
    int height = 1 + fmax(node.getLeftChild().getHeight(), node.getRightChild().getHeight());
    node.setHeight(height);
  }
}

Node rotate_LL(Node oldRoot) {
  Node result = oldRoot.getLeftChild();
  oldRoot.setLeftChild(result.getRightChild());
  result.setRightChild(oldRoot);

  adjustHeight(oldRoot);
  adjustHeight(oldRoot.getLeftChild());
  adjustHeight(result);

  return result;
}

Node rotate_RR(Node oldRoot) {
  Node result = oldRoot.getRightChild();
  oldRoot.setRightChild(result.getLeftChild());
  result.setLeftChild(oldRoot);
  
  adjustHeight(oldRoot);
  adjustHeight(oldRoot.getRightChild());
  adjustHeight(result);

  return result;
}

Node rotate_RL(Node oldRoot) {
  Node rightChild = oldRoot.getRightChild();
  Node result = rightChild.getLeftChild();
  
  oldRoot.setRightChild(result.getLeftChild());
  rightChild.setLeftChild(result.getRightChild());
  
  result.setLeftChild(oldRoot);
  result.setRightChild(rightChild);

  adjustHeight(oldRoot);
  adjustHeight(rightChild);
  adjustHeight(result);

  return result;
}

Node rotate_LR(Node oldRoot) {
  // TODO: Write this method
  throw "Not yet implemented.";
}

class AVLTree {
  private:
    Node root;

    void insertRec(Node newNode) {
      if(this->root == nullptr) return newNode;
      else if ((this->root).getValue() > newNode.getValue()) {
        (this->root).getLeftChild()  
      } else {
      
      }
    }
  public:
    void insert(int value) {
      Node newNode = new Node(value);
      this->root = insertRec(newNode);
      this->root = rebalance(this->root);
      adjustHeight(this->root);
    }
};

int main(int argc, char* argv[]) {
  printf("Hello world");


  return 0;
}
