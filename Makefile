
# TODO: Move `libmongoclient.a` to /usr/local/lib so this can work on production servers
# #
  
CC := g++ # This is the main compiler
# CC := clang --analyze # and comment out the linker last line for sanity
SRCDIR := src
BUILDDIR := build
TARGETDIR := bin
TARGET_1 := avl
TARGET_2 := rbt

SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
CFLAGS := -g -Wall
INC := -I include

$(TARGETDIR)/$(TARGET_1): $(SRCDIR)/$(TARGET_1).cpp
	$(CC) $(CFLAGS) -o $(TARGETDIR)/$(TARGET_1) $(SRCDIR)/$(TARGET_1).$(SRCEXT)
	

clean:
	  @echo " Cleaning..."; 
		  @echo " $(RM) -r $(BUILDDIR) $(TARGET)"; $(RM) -r $(BUILDDIR) $(TARGET)

# Tests
# tester:
#   $(CC) $(CFLAGS) test/tester.cpp $(INC) $(LIB) -o bin/tester
#
#   # Spikes
#ticket:
#	  $(CC) $(CFLAGS) spikes/ticket.cpp $(INC) $(LIB) -o bin/ticket

.PHONY: clean
